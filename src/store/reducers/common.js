import * as actionTypes from '../actions/common';

let initialState ={
  isShowProgress: true
}

function commonReducer(state = initialState, action) {
    switch (action.type) {
      case actionTypes.handleprogress:
        return {
            ...state,
            isShowProgress : action.isShow
        }
      default:
        return state
    }
  }

  export default commonReducer;