import * as actionTypes from '../actions/user';
let initialState = {
  isLoggedIn : false,
  isShowProgress: false,
  userId:0
}

function userReducer(state = initialState, action) {
    switch (action.type) {
      case actionTypes.login:
        localStorage.setItem('userid', action.userId);
        return {
            ...state,
            isLoggedIn : true,
            userId: action.userId
        }
      case actionTypes.logout:
        localStorage.removeItem('userid');
        return {
            ...state,
            isLoggedIn : false
        }
      case actionTypes.islogin:
        const userid = localStorage.getItem('userid') ? localStorage.getItem('userid') : '';
        if(userid !== ''){
          return {
              ...state,
              isLoggedIn : true,
              userId: userid
          }
        }
        else{
          return {
            ...state,
            isLoggedIn : false
        }
        }
      case actionTypes.signup:
        return {
            ...state,
            isLoggedIn : false
      }
      case actionTypes.updateuser:
        return {
            ...state,
            isShowProgress : action.isShow
        }
      default:
        return state
    }
  }

  export default userReducer;