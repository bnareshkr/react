import * as actionTypes from '../actions/product';
let initialState = {
  navProductId:0,
  navcategory:""
}

function productReducer(state = initialState, action) {
    switch (action.type) {
      case actionTypes.getproduct:
        return {
            ...state,
            navProductId: action.id,
            navcategory: action.category
        }
      default:
        return state
    }
  }

  export default productReducer;