
export const getproduct = "GETPRODUCT";


export const getProductAction = (productId, category) =>{
    return {
        type: getproduct,
        id: productId,
        category: category
    }
}

// export const getProductHandle = (productId, category) => {
//     return dispatch => {
//         commonActions.boundHandleProgress(dispatch, true);
//         axios.get(commonActions.baseUrl+`/products?id=`+productId)
//             .then(res => {
//                 commonActions.boundHandleProgress(dispatch, false);
//                 console.log(res,"response");
//                 if(res.data.length>0){
//                     dispatch(getProductAction(res.data[0]));
//                 }
//         }).catch(function (error) {
//             console.log(error);
//             commonActions.boundHandleProgress(dispatch, false);
//           });
//     }
// }

export const boundGetProduct = (dispatch, productId, category) => dispatch(getProductAction(productId, category));