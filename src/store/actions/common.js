
export const handleprogress = "HANDLEPROGRESS";
export const baseUrl = "http://localhost:3004";

export const handleProgressAction = (val) =>{
    return {
        type: handleprogress,
        isShow: val 
    }
}


export const boundHandleProgress = (dispatch, value) => dispatch(handleProgressAction(value)) ;