import axios from 'axios';
import * as commonActions from './common';
import * as userService from '../../services/userService';

export const login = "LOGIN";
export const islogin = "ISLOGIN";
export const logout = "LOGOUT";
export const signup = "SIGNUP";
export const updateuser = "UPDATEUSER";

export const loginAction = (user) =>{
    return {
        type: login,
        userId: user._id
    }
}
export const logoutAction = () =>{
    return {
        type: logout
    }
}
export const isUserLoggedIn = () =>{
    return{
        type: islogin
    }
}
export const updateUserAction = (user) =>{
    return {
        type: updateuser,
        user: user
    }
}
export const validateLogin = (email, password) => {
    return dispatch => {
        commonActions.boundHandleProgress(dispatch, true);
        userService.validateUser(email, password, res =>{
            commonActions.boundHandleProgress(dispatch, false);
                console.log(res,"login response");
                if(res.status === 200){
                    dispatch(loginAction(res.data));
                }
        }, error =>{
            console.log(error, 'error in validate user')
        });
    }
}
export const signupAction=(user)=>{
    return dispatch => {
        commonActions.boundHandleProgress(dispatch, true);
        axios.post(commonActions.baseUrl+"/users", user
        ).then(function (response) {
            console.log(response);
            commonActions.boundHandleProgress(dispatch, false);
          })
          .catch(function (error) {
            console.log(error);
            commonActions.boundHandleProgress(dispatch, false);
          }).catch(function (error) {
            console.log(error);
            commonActions.boundHandleProgress(dispatch, false);
          });
    }
}
export const updateUserHandle = (user) => {
    return dispatch => {
        commonActions.boundHandleProgress(dispatch, true);
        axios.put(commonActions.baseUrl+"/users/"+user.id, user
        ).then(function (response) {
            console.log(response);
            commonActions.boundHandleProgress(dispatch, false);
            updateUserAction(dispatch, user)
          })
          .catch(function (error) {
            console.log(error);
            commonActions.boundHandleProgress(dispatch, false);
          });
    }
}




export const boundLogin = (dispatch, email, password) => dispatch(validateLogin(email,password)) ;
export const boundLogout = (dispatch) => dispatch(logoutAction()) ;
export const boundIsUserLoggedIn = (dispatch) => dispatch(isUserLoggedIn());
export const boundSignup = (dispatch, user) => dispatch(signupAction(user)) ;
export const boundUpdateUser = (dispatch, user) => dispatch(updateUserHandle(user));