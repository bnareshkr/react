import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Layout from './containers/layout/layout';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import userReducer from './store/reducers/user';
import productReducer from './store/reducers/product';
import commonReducer from './store/reducers/common';

const rootReducer = combineReducers({
    userState: userReducer, 
    productState: productReducer, 
    commonState: commonReducer
});

let store = createStore(
    rootReducer, 
    compose(
        applyMiddleware(thunk)
    )
);

ReactDOM.render(<Provider store={store}><Layout /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
