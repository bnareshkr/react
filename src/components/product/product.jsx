import React from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import withStyles from "@material-ui/core/styles/withStyles";
import {
    GridContainer,
    GridItem,
    Card,
    CardAvatar,
    CardBody
  } from '../../utils/uimodule';
import {connect} from 'react-redux';

const styles = {
    cardCategoryWhite: {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    cardTitleWhite: {
      color: "#FFFFFF",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "300",
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: "3px",
      textDecoration: "none"
    }
  };

const Product = (props) =>  {
    const {classes} = props;
    const [product, setProduct] = React.useState({});
    React.useEffect(()=>{
        assignProduct();
    },[]);

    const getApiProducts = () => {
        console.log("Counter", props.match.params);
        axios.get(`http://localhost:3004/products`).then(res => {
            if(res.data.length>0){
                const products = res.data;
                return products.map(productsOfType => {
                    const keys = Object.keys(productsOfType);
                    if(keys[0] === props.match.params.category){
                        return productsOfType[keys[0]].map(product =>{
                            if(product.id === +props.match.params.id){
                                setProduct(product)
                                return null;
                            }
                        });
                    }
                });
            }
        }).catch(error =>{
            console.log(error);
        });
    }
    
   
    const assignProduct=()=>{
        // console.log(getApiProducts());
        return getApiProducts();
        
    }
        return(
            // <p>{props.match.params.id}</p>
            <GridContainer>
               
               <GridItem xs={12} sm={12} md={6} key={product.id+"-grid"}>
                    <Card plain>
                        <CardAvatar plain>
                            <a href="#pablo" onClick={e => e.preventDefault()}>
                                <img src={product.imgUrl} alt="..." />
                            </a>
                        </CardAvatar>
                        <CardBody plain>
                            <h6 className={classes.cardCategory}>{product.productType}</h6>
                            <h4 className={classes.cardTitle}>{product.name}</h4>
                            <p className={classes.description}>
                                Rs: {product.price}
                            </p>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        )
}

const mapStateToProps = state =>{
    return{
        id: state.productState.navProductId,
        category: state.productState.navcategory
    }
}

export default withRouter(withStyles(styles)(connect(mapStateToProps)(Product)));