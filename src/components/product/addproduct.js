import React from 'react';
// import withStyles from "@material-ui/core/styles/withStyles";
import {
    GridContainer,
    GridItem,
    Card,
    CardHeader,
    CardAvatar,
    CardBody,
    CardFooter,
    CustomInput,
    Button
  } from '../../utils/uimodule';
  import { Form, Field } from 'react-final-form';
  import InputField from '../../utils/assets/input';
  
// import asyncValidate from './asyncValidate'

const validate = values => {
  const errors = {}
  const requiredFields = [ 'title', 'product_type', 'price', 'image_url', 'description' ]
  requiredFields.forEach(field => {
    if (!values[ field ]) {
      errors[ field ] = 'Required'
    }
  })
  return errors
}
const productTypes = [
  {key: 'mobile', text: 'Mobile & Accessories', value: 'Mobile & Accessories'},
  {key: 'fashion', text: 'Fashion', value: 'Fashion'},
  {key: 'laptops', text: 'Laptops', value: 'Laptops'},
  {key: 'home_appliances', text: 'Home Appliances', value: 'Home Appliances'}
];

const renderTextField = ({ input, meta, ...custom }) => {
  console.log(meta);
  return (
    <InputField 
      input={input}
      meta={meta}
      options={productTypes} 
      placeholder = {custom.placeholder}
    />
  );
}

// const renderCheckbox = ({ input, label }) => (
//   <Checkbox label={label}
//     checked={input.value ? true : false}
//     onCheck={input.onChange}/>
// )

// const renderRadioGroup = ({ input, ...rest }) => (
//   <RadioButtonGroup {...input} {...rest}
//     valueSelected={input.value}
//     onChange={(event, value) => input.onChange(value)}/>
// )

// const renderSelectField = ({ input, label, meta: { touched, error }, children, ...custom }) => (
//   <SelectField
//     floatingLabelText={label}
//     errorText={touched && error}
//     {...input}
//     onChange={(event, index, value) => input.onChange(value)}
//     children={children}
//     {...custom}/>
// )
const onSubmit = (values) => {
  console.log(values);
}

// const createForm = () =>{
//   const formGroup = [
//     {name: "id", type: "hidden"}, 
//     {name: "title", type: "text", placeholder: "Title"},
//     {name: "product_type", type: "text", placeholder: "Product Type"},
//     {name: "description", type: "text", placeholder: "Description"},
//     {name: "price", type: "number", placeholder: "Price"},
//     {name: "image_url", type: "text", placeholder: "Image URL"}
//   ];
//     return formGroup.map(input => (
//     <GridItem xs={12} sm={12} md={6}>
//       <Field name={input.name} type={input.type} component={renderTextField} placeholder={input.placeholder}
//         subscription={{
//           value: true,
//           active: true,
//           touched: true,
//           error: true,
//           validate: true
//         }}
//       />
//     </GridItem>
//   ) )
// }



const addProduct = props => {
    const { handleSubmit, classes } = props
  
    
    return(
      <GridContainer justify="center" alignItems="center" alignContent="center">
        <GridItem xs={12} sm={12} md={6}>
          <Form  onSubmit={onSubmit}
            validate={validate}
            subscription={{
              submitting: true
            }}
            render={({ handleSubmit, pristine, invalid, submitting }) => (
              <form onSubmit={handleSubmit} style={{width:"100%"}}>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="id" name="id" type="hidden" component={renderTextField} placeholder=""
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="title" name="title" type="text" component={renderTextField} placeholder="Title"
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="product_type" name="product_type" type="select" component={renderTextField} placeholder="Product Type" options={productTypes}
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="price" name="price" type="number" component={renderTextField} placeholder="Price"
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="image_url" name="image_url" type="text" component={renderTextField} placeholder="Image URL"
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Field id="description" name="description" type="text" component={renderTextField} placeholder="Description"
                    subscription={{
                      value: true,
                      active: true,
                      touched: true,
                      error: true,
                      validate: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Button color="primary" type="submit" disabled={pristine || invalid || submitting} >Add Product</Button>
                  {/* <button type="button" disabled={pristine || submitting} onClick={reset}>Clear Values
                  </button> */}
                </GridItem>
              </form>
            )}
            />
        </GridItem>
      </GridContainer>
    )

  }
  export default addProduct;
  