
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import {
  GridContainer,
  GridItem,
  Card,
  CardHeader,
  CardAvatar,
  CardBody,
  CardFooter,
  CustomInput,
  Button
} from '../../utils/uimodule';
import { signupForm } from "../../utils/assets/signup-util";
import {withRouter} from "react-router-dom";
import * as userService from '../../services/userService';
import avatar from "../../assets/img/faces/marc.jpg";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const  UserProfile = (props)=> {
  const { classes } = props;
  const [formInfo, setFormInfo] = React.useState({
    form:{
      first_name: "",
      last_name: "",
      email: "",
      user_name: "",
      city: "",
      country: "",
      about_me: "",
      password: "",
      _id: "" 
    },
    errors:{
      first_name:false,
      last_name:false,
      email:false,
      user_name:false,
      city:false,
      country:false,
      about_me:false,
      password:false
    }
    
  });

  const resetFormData = () =>{
    setFormInfo({
      form:{
        first_name: "",
        last_name: "",
        email: "",
        user_name: "",
        city: "",
        country: "",
        about_me: "",
        password: "",
        _id: "" 
      },
      errors:{
        first_name:false,
        last_name:false,
        email:false,
        user_name:false,
        city:false,
        country:false,
        about_me:false,
        password:false
      }
    });
  }

  useEffect((formInfo)=>{
    if(props.match.params.id !== null && props.match.params.id !== '' && props.match.params.id !== ""){
      userService.getUserById(props.match.params.id, response => {
        console.log('postuser update',response);
        if(+response.status === 200){
          setFormInfo(
            {...formInfo, form: {...response.data}}
          );
        }
      }, error =>{
        console.log('postuser update error',error)
      })
    }
  },[])

  const handleChange = (fieldname, event) => {
    let objFormInfo = {...formInfo.form};
    objFormInfo[fieldname] = event.target.value;
    setFormInfo({ ...formInfo,form: {...objFormInfo}});
  }
  const updateProfileForm =  signupForm.map((input, index) => {
    const mdWidth = input.keyName === "about_me"  ? 12 : 4;
    let formData = {...formInfo.form};
    let formErrorData = {...formInfo.errors};
    let inputProps = {...input.inputProps};
    if(props.location.pathname.includes("profile") && input.keyName.includes("password")){
      return null;
    }
    if(props.location.pathname.includes("profile") && input.keyName.includes("email")){
      inputProps = {...input.inputProps, disabled: true};
    }
      return(
          <GridItem key={input.keyName+"-Grid"} xs={12} sm={12} md={mdWidth}>
                    <CustomInput
                      key={input.keyName}
                      labelText={input.fieldName}
                      id={input.keyName}
                      defaultValue={formData[input.keyName]}
                      error={formErrorData[input.keyName]}
                      keyName={input.keyName}
                      onchange={handleChange}
                      formControlProps={input.formControlProps}
                      inputProps={inputProps}
                    />
            </GridItem>
      )
    
  });

  const handleSignup =() =>{
    let isError = false;
    let objFormInfo = {
      first_name:false,
      last_name:false,
      email:false,
      user_name:false,
      city:false,
      country:false,
      about_me:false,
      password:false
    };
    
     if(formInfo.form.first_name === null || formInfo.form.first_name  === ""){
      objFormInfo.first_name = true;
      isError=true;
     }
     if(formInfo.form.last_name === null || formInfo.form.last_name === ""){
      objFormInfo.last_name = true;
      isError=true;
    }
    if(formInfo.form.email === null || formInfo.form.email === ""){
      objFormInfo.email = true;
      isError=true;
    }
    if((formInfo.form.password === null || formInfo.form.password === "") && props.location.pathname.includes("signup")){
      objFormInfo.password = true;
      isError=true;
    }
    if(formInfo.form.user_name === null || formInfo.form.user_name === ""){
      objFormInfo.user_name = true;
      isError=true;
    }
    if(formInfo.form.city === null || formInfo.form.city === ""){
      objFormInfo.city = true;
      isError=true;
    }
    if(formInfo.form.country === null || formInfo.form.country === ""){
      objFormInfo.country = true;
      isError=true;
    }
    if(formInfo.form.about_me === null || formInfo.form.about_me === ""){
      objFormInfo.about_me = true;
      isError=true;
    }
    setFormInfo({ ...formInfo,errors: {...objFormInfo}});
    if(!isError){
      if(props.location.pathname.includes("signup")){
        userService.addUser(formInfo.form, response => {
          console.log('postuser',response);
          resetFormData();
        }, error =>{
          console.log('postuser error',error)
        });
      }
      else if(props.location.pathname.includes("profile")){
        userService.updateUser(formInfo.form, response =>{
          console.log('postuser update',response);
          // resetFormData();
        }, error =>{
          console.log('postuser update error',error);
        })
      }
      // props.location.pathname.includes("signup") ? props.makeSignup(formInfo.form): props.updateProfile(formInfo.form);
    }
}

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={props.location.pathname.includes("signup") ? 12 : 8} >
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>{props.location.pathname.includes("profile") ? "Edit Profile": "Create Profile"}</h4>
              <p className={classes.cardCategoryWhite}>Complete your profile</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                {updateProfileForm}
              </GridContainer>
              
            </CardBody>
            <CardFooter>
            {props.location.pathname.includes("profile") ?
              <Button color="primary" onClick={handleSignup}>Update Profile</Button>:
              <Button color="primary" onClick={handleSignup}>Create Profile</Button>
            }
            <Button color="primary" onClick={resetFormData}>Clear</Button>
            </CardFooter>
          </Card>
        </GridItem>
        {props.location.pathname.includes("profile") ? 
        <GridItem xs={12} sm={12} md={4}>
          <Card profile>
            <CardAvatar profile>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                <img src={avatar} alt="..." />
              </a>
            </CardAvatar>
            <CardBody profile>
              <h6 className={classes.cardCategory}>{ formInfo.form.user_name }</h6>
              <h4 className={classes.cardTitle}>{formInfo.form.first_name +" "+formInfo.form.last_name}</h4>
              <p className={classes.description}>
               {formInfo.form.about_me}
              </p>
              <Button color="primary" round>
                Follow
              </Button>
            </CardBody>
          </Card>
        </GridItem>
        : null
        }
      </GridContainer>
    </div>
  );
}

UserProfile.propTypes = {
  classes: PropTypes.object
};



export default withRouter(withStyles(styles)(UserProfile));
