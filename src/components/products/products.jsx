import React, {Component} from 'react';
// core components
import {
  GridContainer,
  GridItem,
  Card,
  CardAvatar,
  CardBody
} from '../../utils/uimodule';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionBounds from '../../store/actions/product';

class Products extends Component {
  state={
    products:[],
  }
  classes = {
    cardCategoryWhite: {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    cardTitleWhite: {
      color: "#FFFFFF",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "300",
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: "3px",
      textDecoration: "none"
    }
  };
  productsView;

  componentDidMount(){
    axios.get(`http://localhost:3004/products`).then(res => {
      console.log(res)
      this.setState({products: res.data});
      return null;
    }).catch(error => {
      console.log(error);
    })
  }
  
  productsView = () =>{
    return this.state.products.map(producType => {
      const keys = Object.keys(producType);
      return(
        <GridContainer>
            {this.getEachCategory(producType[keys[0]], keys[0])}
        </GridContainer>
      );
    })
    
  }
  getEachCategory(category, keyName){
    return category.map(product => {
      return(
        <GridItem xs={12} sm={4} md={3} key={product.name.trim()+"-grid"}>
          <Card key={product.name.trim()+"-card"} products onClick={() => { this.props.setProduct(product.id, keyName); this.props.history.push("/product/"+keyName+"/"+product.id) }}>
            <CardAvatar key={product.name.trim()+"-cardavatar"} products>
              <a key={product.name.trim()+"-cardanchor"} onClick={e => e.preventDefault()}>
                <img key={product.name.trim()+"-cardimage"} src={product.imgUrl} alt="..." />
              </a>
            </CardAvatar>
            <CardBody key={product.name.trim()+"-cardbody"} profile>
              <h6 key={product.name.trim()+"-cardh6"} className={this.classes.cardCategory}>{product.productType}</h6>
              <h4 key={product.name.trim()+"-cardh4"} className={this.classes.cardTitle}>{product.name}</h4>
              <p key={product.name.trim()+"-p"} className={this.classes.description}>
              Rs: {product.price}
              </p>
            </CardBody>
          </Card>
        </GridItem>
      )
    });
  }
  render(){ 
    
    return(
      
        <GridContainer>
          {this.productsView()}
        </GridContainer>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    setProduct : (id, category) => actionBounds.boundGetProduct(dispatch, id, category)
  }
}
export default connect(null, mapDispatchToProps)(Products);