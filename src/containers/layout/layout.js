import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { connect } from 'react-redux';

import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Grid';

import Header from '../../components/header/header';
import ProgressBar from '../../utils/assets/loader';
import * as actionBounds from '../../store/actions/user';
import Routes from '../routes/routes';

export const drawerWidth = 240;
export const styles = theme => ({
    content: {
        flexGrow: 1,
        marginTop: '70px',
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: 0,
      },
      contentShift: {
        transition: theme.transitions.create('margin', {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: drawerWidth,
      }
  });

class Layout extends Component{
    state = {
        isLoginModalOpen: false,
        isSideMenuOpen: false
    }
    componentDidMount(){
        this.props.isUserLoggedIn();
    }
    

    handleLoginModalOpen = () =>{
        this.setState({isLoginModalOpen :true});
    }
    handleLoginModalClose = () =>{
        this.setState({isLoginModalOpen :false});
    }
    handleLogin = (username,password) =>{
        console.log(username,"response");
        const res = this.props.makeLogin(username,password);
        console.log(res, "[Dispatch Response after login]")
        this.setState({
            isLoginModalOpen :false
        });
    }
    handleLogout = () =>{
        this.props.makeLogout();
        this.setState({
            isLoginModalOpen :false
            // isLoginButton: true
        });
    }
    handleSideMenu = (isOpen) => {
        this.setState({isSideMenuOpen: isOpen});
    }
    
     
    render(){
        const {classes} = this.props;
        return(
            <React.Fragment>
                <Router>
               <div >
                    <Header 
                        handleSideMenu={ this.handleSideMenu } 
                        isLoginButton={!this.props.isLoggedIn} 
                        handleLogin={this.handleLogin} 
                        handleLogout={this.handleLogout} 
                        isModalOpen={this.state.isLoginModalOpen} 
                        handleClose={this.handleLoginModalClose} 
                        handleOpen={this.handleLoginModalOpen}
                    >
                    </Header>
                    {this.props.isShowProgress ? <ProgressBar></ProgressBar>: null}
                    </div>
               <Container>
                    <main
                        className={clsx(classes.content, {
                            [classes.contentShift]: this.state.isSideMenuOpen,
                            })}
                    >
                       <Routes />
                    </main>
                    </Container>
                </Router>
            </React.Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        isLoggedIn: state.userState.isLoggedIn,
        isShowProgress: state.commonState.isShowProgress
    }
}
const mapDispatchToProps = dispatch => {
    return{
        makeLogin : (username,password) => { actionBounds.boundLogin(dispatch, username,password)},
        makeLogout : () => { actionBounds.boundLogout(dispatch)},
        isUserLoggedIn : () => { actionBounds.boundIsUserLoggedIn(dispatch)}
    }

}
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(Layout));
