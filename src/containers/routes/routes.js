import React, {lazy, Suspense} from 'react';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';
const UserProfile = React.lazy(() => import('../../components/UserProfile/UserProfile'));
const Products = React.lazy(() => import('../../components/products/products'));
const Product = React.lazy(() => import('../../components/product/product'));
const AddProduct = React.lazy(() => import('../../components/product/addproduct'));



const routes = (props) =>{

    const addRoutes =() => props.isLoggedIn ?
                <React.Fragment>
                    <Suspense fallback="<h2>Loading....</h2>">
                        <Route exact key="profile" path="/profile/:id" component={UserProfile} /> 
                        <Route exact key="addproduct" path="/addproduct" component={AddProduct} /> 
                        <Route exact key="products" path="/products" component={Products} />
                        <Route exact key="product" path="/product/:category/:id" component={Product} />
                        <Route exact key="productsifemptyroute" path="/" component={Products} />
                        <Route exact key="productsifincorrectpath" path="*" component={Products} />
                    </Suspense>
                </React.Fragment>
        :
                <React.Fragment>
                    <Suspense fallback="<h2>Loading....</h2>">
                        <Route exact key="signup" path="/signup" component={UserProfile} />
                        <Route exact key="products" path="/products" component={Products} />
                        <Route exact key="product" path="/product/:category/:id" component={Product} />
                        <Route exact key="productsifemptyroute" path="/" component={Products} />
                        <Route exact key="productsifincorrectpath" path="*" component={Products} />
                    </Suspense>
                </React.Fragment>

    return(
        addRoutes()
    )
}

const mapStateToProps = state => {
    return{
        isLoggedIn: state.userState.isLoggedIn
    }
}
export default connect(mapStateToProps)(routes);
