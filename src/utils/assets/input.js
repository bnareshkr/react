import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { switchCase } from "@babel/types";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing(1)
  },
  hide: {
    display: "none"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function InputField(props) {
  const classes = useStyles();

  const getInput = (type, props) => {
    switch (type) {
      case 'text':
        return (<Input {...props.input} />);
      case 'select':
        return (
          <Select
              {...props.input}
              className={classes.selectEmpty}
            >
            { props.options.map(option => 
              <MenuItem value={option.value}>{option.text}</MenuItem>
            )}
              
            </Select>
        );
      case 'text':
        return (<Input {...props.input} underline={false} />);
        
      default:
        return (<Input {...props.input} />);
    }
  }
  return (
      <FormControl className={classes.formControl} error={props.meta.touched && props.meta.invalid} fullWidth={true}>
        {props.input.type !== 'hidden' ? <InputLabel htmlFor={props.input.name}>{props.placeholder}</InputLabel> : null }
         {getInput(props.input.type, props)}
          
         { props.input.type !== 'hidden' 
         ?  <FormHelperText
            className={props.meta.touched && props.meta.invalid ? "": classes.hide}
            >
          {props.meta.error}
        </FormHelperText>
        : null }
      </FormControl>
  );
}