export const signupForm = [
    {
        fieldName: "First Name",
        keyName:"first_name",
        fieldValue: '',
        formControlProps: {
            fullWidth: true,
        },
        inputProps: {}
    },
    {
        fieldName: "Last Name",
        keyName:"last_name",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "User Name",
        keyName:"user_name",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "Email",
        keyName:"email",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "Password",
        keyName:"password",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "City",
        keyName:"city",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "Country",
        keyName:"country",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {}
    },
    {
        fieldName: "About Me",
        keyName:"about_me",
        fieldValue: '',
        formControlProps: {
            fullWidth: true
        },
        inputProps: {
            multiline: true,
            rows: 5
        }
    },
]