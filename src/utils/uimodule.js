import GridContainer from "./Grid/GridContainer.jsx";
import GridItem from "./Grid/GridItem.jsx";
import Card from "./Card/Card.jsx";
import CardHeader from "./Card/CardHeader.jsx";
import CardAvatar from "./Card/CardAvatar.jsx";
import CardBody from "./Card/CardBody.jsx";
import CardFooter from "./Card/CardFooter.jsx";
import CustomInput from "./CustomInput/CustomInput.jsx";
import Button from "./CustomButtons/Button.jsx";

const uimodules = {
    GridContainer,
    GridItem,
    Card,
    CardHeader,
    CardAvatar,
    CardBody,
    CardFooter,
    CustomInput,
    Button
 }

 export { default as GridContainer } from './Grid/GridContainer.jsx';
 export { default as GridItem }    from './Grid/GridItem.jsx';
 export { default as Card }        from './Card/Card.jsx';
 export { default as CardHeader }  from './Card/CardHeader.jsx';
 export { default as CardAvatar }  from './Card/CardAvatar.jsx';
 export { default as CardBody }    from './Card/CardBody.jsx';
 export { default as CardFooter }  from './Card/CardFooter.jsx';
 export { default as CustomInput } from './CustomInput/CustomInput.jsx';
 export { default as Button }      from './CustomButtons/Button.jsx';