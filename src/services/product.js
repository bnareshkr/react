import axios from 'axios';
const baseUrl = `http://localhost:8080`;

export const getProductById = (id, cb) => {
    axios.get(baseUrl+`/shop/getProductById/`+id).then((response)=>{
        console.log('getProductById',response);
        cb(response);
      }).catch(error =>{
        console.log('getProductById',error);
        cb(error);
      });
}

export const getAllProducts = (cb) => {
    axios.post(baseUrl+`/shop/getAllProducts`,).then((response)=>{
        console.log('getAllProducts',response);
        cb(response);
      }).catch(error =>{
        console.log('getAllProducts error',error);
        cb(error);
      });
}

export const addProduct = (productObj, cb) => {
    axios.post(baseUrl+`/shop/addProduct`, productObj).then((response)=>{
        cb(response);
      }).catch(error =>{
        console.log('addProduct error',error);
        cb(error);
      });
}

export const updateProduct = (userObj, cb) => {
    axios.post(baseUrl+`/shop/updateProduct`, userObj).then((response)=>{
        cb(response);
      }).catch(error =>{
        console.log('updateProduct',error);
        cb(error);
      });
}