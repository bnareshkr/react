import axios from 'axios';
import {toast} from 'react-toastify';
const baseUrl = `http://localhost:8080`;

export const getUserById = (id, cb) => {
    axios.get(baseUrl+`/account/getUserById/`+id).then((response)=>{
        console.log('get user',response);
        cb(response);
      }).catch(error =>{
        console.log('get user error',error);
        cb(error);
      });
}

export const validateUser = (username, password, cb) => {
    axios.post(baseUrl+`/account/validateUserLogin`, {email: username, password: password}).then((response)=>{
        console.log('login user',response);
        cb(response);
      }).catch(error =>{
        console.log('validateUser error',error);
        cb(error);
      });
}

export const addUser = (userObj, cb) => {
    axios.post(baseUrl+`/account/addUser`, userObj).then((response)=>{
      toast.info("User added Succesfully");
        cb(response);
      }).catch(error =>{
        console.log('addUser error',error);
        cb(error);
      });
}

export const updateUser = (userObj, cb) => {
    axios.post(baseUrl+`/account/updateUser`, userObj).then((response)=>{
      toast.info("User Updation Succesfully");
        cb(response);
      }).catch(error =>{
        console.log('updateUser error',error);
        cb(error);
      });
}